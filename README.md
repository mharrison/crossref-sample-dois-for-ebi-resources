# Crossref Sample DOIs for EBI resources

This repository contains sample XML files for the EBI resources to reference when creating Crossref DOI submissions for:
- Database updates
- Data files submitted by external authors
- Training materials

The XML schema used can be found here: https://www.crossref.org/documentation/schema-library/metadata-deposit-schema-5-3-1/

